package br.com.summerxp.hrpayroll.services;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.summerxp.hrpayroll.entities.Payment;
import br.com.summerxp.hrpayroll.entities.Worker;
import br.com.summerxp.hrpayroll.feignclients.WorkerFeignClient;

@Service
public class PaymentService {

	@Value("${hr-worker.host}")
	private String workerHost;

	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private WorkerFeignClient workerFeignClient;
	
	public Payment getPaymentRestTemplate(Long workId, Integer days) {
		
		Map<String, String> uriVariable = new HashMap<>();
		uriVariable.put("id", workId.toString());
		
		Worker worker = restTemplate.getForObject(workerHost + "/workers/{id}", Worker.class, uriVariable);
		
		if(worker != null) {
			return new Payment(worker.getName(), worker.getDailyIncome(), days);
		}
		
		return new Payment("", new BigDecimal(0), 0);
	}
	
	public Payment getPaymentFeign(Long workId, Integer days) {
		
		Worker worker = workerFeignClient.findById(workId).getBody();
		
		if(worker != null) {
			return new Payment(worker.getName(), worker.getDailyIncome(), days);
		}
		
		return new Payment("", new BigDecimal(0), 0);
	}
}
