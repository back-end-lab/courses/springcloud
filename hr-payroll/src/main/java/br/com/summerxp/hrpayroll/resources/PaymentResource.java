package br.com.summerxp.hrpayroll.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.summerxp.hrpayroll.entities.Payment;
import br.com.summerxp.hrpayroll.services.PaymentService;

@RestController
@RequestMapping(value = "/payments")
public class PaymentResource {

	@Autowired
	private PaymentService paymentService;
	
	@GetMapping(value = "/{id}/days/{qtd}")
	public ResponseEntity<Payment> getPayment(@PathVariable Long id, @PathVariable Integer qtd){
		Payment payment = paymentService.getPaymentFeign(id, qtd);
		return ResponseEntity.ok(payment);
	}
}
