package br.com.summerxp.hrworker.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.summerxp.hrworker.entities.Worker;
import br.com.summerxp.hrworker.repositories.WorkerRepository;

@Service
public class WorkerService {

	@Autowired
	private WorkerRepository workerRepository;

	public List<Worker> findAll() {
		List<Worker> worker = workerRepository.findAll();
		return worker;
	}

	public Worker findById(Long id) {
		Optional<Worker> worker = workerRepository.findById(id);
		return worker.get();
	}

	public Worker saveWorker(Worker worker) {
		Worker workerResult = workerRepository.save(worker);
		return workerResult;
	}

	public void deleteWorker(Long id) {

		Optional<Worker> worker = workerRepository.findById(id);

		workerRepository.delete(worker.get());
	}

//	public Map<String, Boolean> deleteWorker(Long id) {
//		Optional<Worker> worker = workerRepository.findById(id);
//		workerRepository.delete(worker.get());
//		Map<String, Boolean> response = new HashMap<>();
//		response.put("deleted", Boolean.TRUE);
//
//		return response;
//	}
	
}
