package br.com.summerxp.hrworker.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.summerxp.hrworker.entities.Worker;
import br.com.summerxp.hrworker.services.WorkerService;

@RestController
@RequestMapping(value = "/workers")
public class WorkerController {

	@Autowired
	private WorkerService workerService;

	@GetMapping
	public ResponseEntity<List<Worker>> finAll() {

//		List<Worker> workersList = Arrays.asList(
//				new Worker(1L, "Bob", new BigDecimal(200.0)),
//				new Worker(2L, "Maria", new BigDecimal(300.0)),
//				new Worker(3L, "Alex", new BigDecimal(250.0)));
		List<Worker> workersList = workerService.findAll();
		return ResponseEntity.ok(workersList);
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<Worker> findById(@PathVariable Long id) {
//		Worker workerResult = new Worker(1L, "Bob", new BigDecimal(200.0));
		Worker workerResult = workerService.findById(id);
		return ResponseEntity.ok(workerResult);
	}

	@PostMapping
	public ResponseEntity<Worker> createUser(@RequestHeader(value = "api-key") String string,
			@RequestBody Worker worker) {
		workerService.saveWorker(worker);
		URI location = URI.create(String.format("/workers/%s", worker.getId()));
		return ResponseEntity.created(location).body(worker);
	}

	@PutMapping(value = "/{id}")
	public ResponseEntity<Worker> updateWorker(@PathVariable Long id, @RequestBody Worker updateWorker) {
		Worker worker = workerService.findById(id);

		worker.setName(updateWorker.getName());
		worker.setDailyIncome(updateWorker.getDailyIncome());

		final Worker workerResult = workerService.saveWorker(worker);
		return ResponseEntity.ok(workerResult);
	}

	@PatchMapping("/{id}/name/{newName}")
	public ResponseEntity<Worker> patchWorker(@PathVariable Long id, @PathVariable String newName) {
		try {
			Worker worker = workerService.findById(id);
			worker.setName(newName);

			Worker workerResult = workerService.saveWorker(worker);

			return ResponseEntity.ok(workerResult);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deletWorker(@PathVariable Long id) {
		workerService.deleteWorker(id);
		return ResponseEntity.noContent().build();
	}
}