package br.com.summerxp.hrworker.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.summerxp.hrworker.entities.Worker;

public interface WorkerRepository extends JpaRepository<Worker, Long>{

}
